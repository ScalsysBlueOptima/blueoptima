<%@page import="com.liferay.portal.kernel.captcha.CaptchaTextException"%>
<%@page import="com.liferay.portal.kernel.captcha.CaptchaMaxChallengesException"%>

<%@include file="init.jsp"%>

	<div class="btmcolheader">EMAIL US</div>
	<div id="successMessage" class="portlet-msg-success" style="display: none;"></div>
	<div id="errorMessage" class="portlet-msg-error" style="display: none;"></div>
	<div id="InvalidCaptcha" class="portlet-msg-error" style="display: none;"></div>
	<div id="enterCaptcha" class="portlet-msg-error" style="display: none;"></div>
		<form  method="post" name="<portlet:namespace/>fm" id="<portlet:namespace/>fm">
			<div class="formfild clearfix">
					<input type="text" class="btmtextfild" placeholder="name" id='<portlet:namespace/>name' name='<portlet:namespace/>name' value="${emailUs.name}">
			</div>
			<div class="formfild clearfix">
					<input type="text" placeholder="email" id='<portlet:namespace/>email' name='<portlet:namespace/>email' value="${emailUs.email}" class="btmtextfild">
			</div>
			<div class="formfild clearfix">
					<input type="text" placeholder="subject" id='<portlet:namespace/>subject' name='<portlet:namespace/>subject' value="${emailUs.subject}" class="btmtextfild">
			</div>
			<div class="formfild clearfix emailcaptcha">
					<liferay-ui:captcha url="<%=captchaURL%>" />
			</div>
			<div class="formfild clearfix">
				<textarea id='<portlet:namespace/>message' name='<portlet:namespace/>message' class="btmtextareafild">${emailUs.message}</textarea>
			</div>
			<div class="formfild clearfix">	
				<input class="sendbtn" type="submit"  value="<liferay-ui:message key='submit' />"  />
			</div>
		</form>
		
<%@include file="view_js.jsp"%>




