<%@ include file="init.jsp"%>
<script type="text/javascript">

	
	  // When the document is ready
    $(document).ready(function () {
        //validation rules
        $("#<portlet:namespace/>fm").validate({
            rules: {
            	"<portlet:namespace/>name": {
                    required: true,
                    },  
                "<portlet:namespace/>email": {
                    required: true,
                    minlength: 5,
                    email:true
                },
                "<portlet:namespace/>subject":{
                	 required: true,
                    
                },
                "<portlet:namespace/>message":{
                	 required: true,

                },
            },
            submitHandler: function() {
            	  var name=document.getElementById('<portlet:namespace />name').value;
            	  var email=document.getElementById('<portlet:namespace />email').value;
            	  var subject=document.getElementById('<portlet:namespace />subject').value;
            	  var message=document.getElementById('<portlet:namespace />message').value;
            	  var captchaText=document.getElementById('<portlet:namespace />captchaText').value;
            	jQuery.ajax({
    				url  : '<%=sendMailURL%>',
    				type : 'POST',
    				datatype:"JSON",
    				data:{
    					"name":name,
    					"email":email,
    					"subject":subject,
    					"message":message,
    					"captchaText":captchaText,
    				},
    				success : function(response) {
    					var result = jQuery.parseJSON(response);
    						$('#<portlet:namespace />fm').find("input[type=text], textarea").val("");
    						 $(".captcha").attr("src", $(".captcha").attr("src")+"&force=" + new Date().getMilliseconds()); 
    						if(result.status == "success"){
    							$('#errorMessage').hide();
    							 $('#InvalidCaptcha').hide();
    							 $('#enterCaptcha').hide();
    							 $('#successMessage').html('<liferay-ui:message key="profile-status-value-success-message" />').show();
    							 return true;
    						 }else if(result.status == "error"){
    							$('#successMessage').hide();
    							$('#InvalidCaptcha').hide();
    							$('#enterCaptcha').hide();
    							$('#errorMessage').html('<liferay-ui:message key="profile-status-value-error-message" />').show();
    						
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>name',result.emailObj.name);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>subject',result.emailObj.subject);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>message',result.emailObj.message);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>email',result.emailObj.email);
    							 return  false;
    						}
    						 else if(result.status == "InvalidCaptcha"){
    							 $('#errorMessage').hide();
    							 $('#successMessage').hide();
    							 $('#enterCaptcha').hide();
    							 $('#InvalidCaptcha').html('<liferay-ui:message key="profile-status-value-invaid-captcha" />').show();
    							 
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>name',result.emailObj.name);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>subject',result.emailObj.subject);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>message',result.emailObj.message);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>email',result.emailObj.email);
    							 return false;
    						 }else if(result.status == "entercaptcha"){
    							 $('#errorMessage').hide();
    							 $('#successMessage').hide();
    							 $('#InvalidCaptcha').hide();
    							 $('#enterCaptcha').html('<liferay-ui:message key="profile-status-value-enter-captcha" />').show();
    							 
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>name',result.emailObj.name);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>subject',result.emailObj.subject);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>message',result.emailObj.message);
    							 <portlet:namespace/>fillEmailObject('<portlet:namespace/>email',result.emailObj.email);
    							 return false;
    						 }
    					}
    				
            	
            	});
            	}
        });
        });
 
 $(document).ready(function(){
	document.getElementsByName('<portlet:namespace/>captchaText')[0].placeholder='enter code';
	 $("#<portlet:namespace/>fm").validate(); 
});
<portlet:namespace/>fillEmailObject = function(id,value){
	if(value != ''){
		$('#'+id).val(value);
	}
}
</script>
<style>
.error{
color:red !important;
}
.validate{
color:red !important;
}
</style>

