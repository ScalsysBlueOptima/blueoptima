/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.service.persistence;

import com.blueoptima.model.EmailUs;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the email us service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Scalsysu9
 * @see EmailUsPersistenceImpl
 * @see EmailUsUtil
 * @generated
 */
public interface EmailUsPersistence extends BasePersistence<EmailUs> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link EmailUsUtil} to access the email us persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the email us in the entity cache if it is enabled.
	*
	* @param emailUs the email us
	*/
	public void cacheResult(com.blueoptima.model.EmailUs emailUs);

	/**
	* Caches the email uses in the entity cache if it is enabled.
	*
	* @param emailUses the email uses
	*/
	public void cacheResult(
		java.util.List<com.blueoptima.model.EmailUs> emailUses);

	/**
	* Creates a new email us with the primary key. Does not add the email us to the database.
	*
	* @param id the primary key for the new email us
	* @return the new email us
	*/
	public com.blueoptima.model.EmailUs create(long id);

	/**
	* Removes the email us with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the email us
	* @return the email us that was removed
	* @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.blueoptima.model.EmailUs remove(long id)
		throws com.blueoptima.NoSuchEmailUsException,
			com.liferay.portal.kernel.exception.SystemException;

	public com.blueoptima.model.EmailUs updateImpl(
		com.blueoptima.model.EmailUs emailUs)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the email us with the primary key or throws a {@link com.blueoptima.NoSuchEmailUsException} if it could not be found.
	*
	* @param id the primary key of the email us
	* @return the email us
	* @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.blueoptima.model.EmailUs findByPrimaryKey(long id)
		throws com.blueoptima.NoSuchEmailUsException,
			com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the email us with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param id the primary key of the email us
	* @return the email us, or <code>null</code> if a email us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.blueoptima.model.EmailUs fetchByPrimaryKey(long id)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the email uses.
	*
	* @return the email uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.blueoptima.model.EmailUs> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the email uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email uses
	* @param end the upper bound of the range of email uses (not inclusive)
	* @return the range of email uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.blueoptima.model.EmailUs> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the email uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email uses
	* @param end the upper bound of the range of email uses (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of email uses
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.blueoptima.model.EmailUs> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the email uses from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of email uses.
	*
	* @return the number of email uses
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}