/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.service.persistence;

import com.blueoptima.model.EmailUs;

import com.blueoptima.service.EmailUsLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Scalsysu9
 * @generated
 */
public abstract class EmailUsActionableDynamicQuery
	extends BaseActionableDynamicQuery {
	public EmailUsActionableDynamicQuery() throws SystemException {
		setBaseLocalService(EmailUsLocalServiceUtil.getService());
		setClass(EmailUs.class);

		setClassLoader(com.blueoptima.service.ClpSerializer.class.getClassLoader());

		setPrimaryKeyPropertyName("id");
	}
}