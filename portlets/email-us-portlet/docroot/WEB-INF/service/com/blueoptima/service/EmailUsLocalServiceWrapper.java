/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EmailUsLocalService}.
 *
 * @author Scalsysu9
 * @see EmailUsLocalService
 * @generated
 */
public class EmailUsLocalServiceWrapper implements EmailUsLocalService,
	ServiceWrapper<EmailUsLocalService> {
	public EmailUsLocalServiceWrapper(EmailUsLocalService emailUsLocalService) {
		_emailUsLocalService = emailUsLocalService;
	}

	/**
	* Adds the email us to the database. Also notifies the appropriate model listeners.
	*
	* @param emailUs the email us
	* @return the email us that was added
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.blueoptima.model.EmailUs addEmailUs(
		com.blueoptima.model.EmailUs emailUs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.addEmailUs(emailUs);
	}

	/**
	* Creates a new email us with the primary key. Does not add the email us to the database.
	*
	* @param id the primary key for the new email us
	* @return the new email us
	*/
	@Override
	public com.blueoptima.model.EmailUs createEmailUs(long id) {
		return _emailUsLocalService.createEmailUs(id);
	}

	/**
	* Deletes the email us with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param id the primary key of the email us
	* @return the email us that was removed
	* @throws PortalException if a email us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.blueoptima.model.EmailUs deleteEmailUs(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.deleteEmailUs(id);
	}

	/**
	* Deletes the email us from the database. Also notifies the appropriate model listeners.
	*
	* @param emailUs the email us
	* @return the email us that was removed
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.blueoptima.model.EmailUs deleteEmailUs(
		com.blueoptima.model.EmailUs emailUs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.deleteEmailUs(emailUs);
	}

	@Override
	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _emailUsLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@Override
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.dynamicQueryCount(dynamicQuery);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @param projection the projection to apply to the query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
		com.liferay.portal.kernel.dao.orm.Projection projection)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.dynamicQueryCount(dynamicQuery, projection);
	}

	@Override
	public com.blueoptima.model.EmailUs fetchEmailUs(long id)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.fetchEmailUs(id);
	}

	/**
	* Returns the email us with the primary key.
	*
	* @param id the primary key of the email us
	* @return the email us
	* @throws PortalException if a email us with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.blueoptima.model.EmailUs getEmailUs(long id)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.getEmailUs(id);
	}

	@Override
	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the email uses.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	* </p>
	*
	* @param start the lower bound of the range of email uses
	* @param end the upper bound of the range of email uses (not inclusive)
	* @return the range of email uses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public java.util.List<com.blueoptima.model.EmailUs> getEmailUses(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.getEmailUses(start, end);
	}

	/**
	* Returns the number of email uses.
	*
	* @return the number of email uses
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public int getEmailUsesCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.getEmailUsesCount();
	}

	/**
	* Updates the email us in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param emailUs the email us
	* @return the email us that was updated
	* @throws SystemException if a system exception occurred
	*/
	@Override
	public com.blueoptima.model.EmailUs updateEmailUs(
		com.blueoptima.model.EmailUs emailUs)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _emailUsLocalService.updateEmailUs(emailUs);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	@Override
	public java.lang.String getBeanIdentifier() {
		return _emailUsLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	@Override
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_emailUsLocalService.setBeanIdentifier(beanIdentifier);
	}

	@Override
	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _emailUsLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
	 */
	public EmailUsLocalService getWrappedEmailUsLocalService() {
		return _emailUsLocalService;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
	 */
	public void setWrappedEmailUsLocalService(
		EmailUsLocalService emailUsLocalService) {
		_emailUsLocalService = emailUsLocalService;
	}

	@Override
	public EmailUsLocalService getWrappedService() {
		return _emailUsLocalService;
	}

	@Override
	public void setWrappedService(EmailUsLocalService emailUsLocalService) {
		_emailUsLocalService = emailUsLocalService;
	}

	private EmailUsLocalService _emailUsLocalService;
}