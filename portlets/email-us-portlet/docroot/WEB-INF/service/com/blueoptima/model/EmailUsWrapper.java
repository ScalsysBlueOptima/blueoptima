/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link EmailUs}.
 * </p>
 *
 * @author Scalsysu9
 * @see EmailUs
 * @generated
 */
public class EmailUsWrapper implements EmailUs, ModelWrapper<EmailUs> {
	public EmailUsWrapper(EmailUs emailUs) {
		_emailUs = emailUs;
	}

	@Override
	public Class<?> getModelClass() {
		return EmailUs.class;
	}

	@Override
	public String getModelClassName() {
		return EmailUs.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("name", getName());
		attributes.put("email", getEmail());
		attributes.put("subject", getSubject());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String subject = (String)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	/**
	* Returns the primary key of this email us.
	*
	* @return the primary key of this email us
	*/
	@Override
	public long getPrimaryKey() {
		return _emailUs.getPrimaryKey();
	}

	/**
	* Sets the primary key of this email us.
	*
	* @param primaryKey the primary key of this email us
	*/
	@Override
	public void setPrimaryKey(long primaryKey) {
		_emailUs.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the ID of this email us.
	*
	* @return the ID of this email us
	*/
	@Override
	public long getId() {
		return _emailUs.getId();
	}

	/**
	* Sets the ID of this email us.
	*
	* @param id the ID of this email us
	*/
	@Override
	public void setId(long id) {
		_emailUs.setId(id);
	}

	/**
	* Returns the name of this email us.
	*
	* @return the name of this email us
	*/
	@Override
	public java.lang.String getName() {
		return _emailUs.getName();
	}

	/**
	* Sets the name of this email us.
	*
	* @param name the name of this email us
	*/
	@Override
	public void setName(java.lang.String name) {
		_emailUs.setName(name);
	}

	/**
	* Returns the email of this email us.
	*
	* @return the email of this email us
	*/
	@Override
	public java.lang.String getEmail() {
		return _emailUs.getEmail();
	}

	/**
	* Sets the email of this email us.
	*
	* @param email the email of this email us
	*/
	@Override
	public void setEmail(java.lang.String email) {
		_emailUs.setEmail(email);
	}

	/**
	* Returns the subject of this email us.
	*
	* @return the subject of this email us
	*/
	@Override
	public java.lang.String getSubject() {
		return _emailUs.getSubject();
	}

	/**
	* Sets the subject of this email us.
	*
	* @param subject the subject of this email us
	*/
	@Override
	public void setSubject(java.lang.String subject) {
		_emailUs.setSubject(subject);
	}

	/**
	* Returns the message of this email us.
	*
	* @return the message of this email us
	*/
	@Override
	public java.lang.String getMessage() {
		return _emailUs.getMessage();
	}

	/**
	* Sets the message of this email us.
	*
	* @param message the message of this email us
	*/
	@Override
	public void setMessage(java.lang.String message) {
		_emailUs.setMessage(message);
	}

	@Override
	public boolean isNew() {
		return _emailUs.isNew();
	}

	@Override
	public void setNew(boolean n) {
		_emailUs.setNew(n);
	}

	@Override
	public boolean isCachedModel() {
		return _emailUs.isCachedModel();
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_emailUs.setCachedModel(cachedModel);
	}

	@Override
	public boolean isEscapedModel() {
		return _emailUs.isEscapedModel();
	}

	@Override
	public java.io.Serializable getPrimaryKeyObj() {
		return _emailUs.getPrimaryKeyObj();
	}

	@Override
	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_emailUs.setPrimaryKeyObj(primaryKeyObj);
	}

	@Override
	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _emailUs.getExpandoBridge();
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.model.BaseModel<?> baseModel) {
		_emailUs.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
		_emailUs.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_emailUs.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new EmailUsWrapper((EmailUs)_emailUs.clone());
	}

	@Override
	public int compareTo(com.blueoptima.model.EmailUs emailUs) {
		return _emailUs.compareTo(emailUs);
	}

	@Override
	public int hashCode() {
		return _emailUs.hashCode();
	}

	@Override
	public com.liferay.portal.model.CacheModel<com.blueoptima.model.EmailUs> toCacheModel() {
		return _emailUs.toCacheModel();
	}

	@Override
	public com.blueoptima.model.EmailUs toEscapedModel() {
		return new EmailUsWrapper(_emailUs.toEscapedModel());
	}

	@Override
	public com.blueoptima.model.EmailUs toUnescapedModel() {
		return new EmailUsWrapper(_emailUs.toUnescapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _emailUs.toString();
	}

	@Override
	public java.lang.String toXmlString() {
		return _emailUs.toXmlString();
	}

	@Override
	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_emailUs.persist();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailUsWrapper)) {
			return false;
		}

		EmailUsWrapper emailUsWrapper = (EmailUsWrapper)obj;

		if (Validator.equals(_emailUs, emailUsWrapper._emailUs)) {
			return true;
		}

		return false;
	}

	/**
	 * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
	 */
	public EmailUs getWrappedEmailUs() {
		return _emailUs;
	}

	@Override
	public EmailUs getWrappedModel() {
		return _emailUs;
	}

	@Override
	public void resetOriginalValues() {
		_emailUs.resetOriginalValues();
	}

	private EmailUs _emailUs;
}