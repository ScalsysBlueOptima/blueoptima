/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.blueoptima.service.http.EmailUsServiceSoap}.
 *
 * @author Scalsysu9
 * @see com.blueoptima.service.http.EmailUsServiceSoap
 * @generated
 */
public class EmailUsSoap implements Serializable {
	public static EmailUsSoap toSoapModel(EmailUs model) {
		EmailUsSoap soapModel = new EmailUsSoap();

		soapModel.setId(model.getId());
		soapModel.setName(model.getName());
		soapModel.setEmail(model.getEmail());
		soapModel.setSubject(model.getSubject());
		soapModel.setMessage(model.getMessage());

		return soapModel;
	}

	public static EmailUsSoap[] toSoapModels(EmailUs[] models) {
		EmailUsSoap[] soapModels = new EmailUsSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static EmailUsSoap[][] toSoapModels(EmailUs[][] models) {
		EmailUsSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new EmailUsSoap[models.length][models[0].length];
		}
		else {
			soapModels = new EmailUsSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static EmailUsSoap[] toSoapModels(List<EmailUs> models) {
		List<EmailUsSoap> soapModels = new ArrayList<EmailUsSoap>(models.size());

		for (EmailUs model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new EmailUsSoap[soapModels.size()]);
	}

	public EmailUsSoap() {
	}

	public long getPrimaryKey() {
		return _id;
	}

	public void setPrimaryKey(long pk) {
		setId(pk);
	}

	public long getId() {
		return _id;
	}

	public void setId(long id) {
		_id = id;
	}

	public String getName() {
		return _name;
	}

	public void setName(String name) {
		_name = name;
	}

	public String getEmail() {
		return _email;
	}

	public void setEmail(String email) {
		_email = email;
	}

	public String getSubject() {
		return _subject;
	}

	public void setSubject(String subject) {
		_subject = subject;
	}

	public String getMessage() {
		return _message;
	}

	public void setMessage(String message) {
		_message = message;
	}

	private long _id;
	private String _name;
	private String _email;
	private String _subject;
	private String _message;
}