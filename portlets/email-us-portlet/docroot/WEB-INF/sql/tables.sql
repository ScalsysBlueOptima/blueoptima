create table email_us (
	id_ LONG not null primary key,
	name VARCHAR(75) null,
	email VARCHAR(75) null,
	subject VARCHAR(75) null,
	message VARCHAR(75) null
);