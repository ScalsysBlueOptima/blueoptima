/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.service.persistence;

import com.blueoptima.NoSuchEmailUsException;

import com.blueoptima.model.EmailUs;
import com.blueoptima.model.impl.EmailUsImpl;
import com.blueoptima.model.impl.EmailUsModelImpl;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the email us service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Scalsysu9
 * @see EmailUsPersistence
 * @see EmailUsUtil
 * @generated
 */
public class EmailUsPersistenceImpl extends BasePersistenceImpl<EmailUs>
	implements EmailUsPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link EmailUsUtil} to access the email us persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = EmailUsImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsModelImpl.FINDER_CACHE_ENABLED, EmailUsImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsModelImpl.FINDER_CACHE_ENABLED, EmailUsImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);

	public EmailUsPersistenceImpl() {
		setModelClass(EmailUs.class);
	}

	/**
	 * Caches the email us in the entity cache if it is enabled.
	 *
	 * @param emailUs the email us
	 */
	@Override
	public void cacheResult(EmailUs emailUs) {
		EntityCacheUtil.putResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsImpl.class, emailUs.getPrimaryKey(), emailUs);

		emailUs.resetOriginalValues();
	}

	/**
	 * Caches the email uses in the entity cache if it is enabled.
	 *
	 * @param emailUses the email uses
	 */
	@Override
	public void cacheResult(List<EmailUs> emailUses) {
		for (EmailUs emailUs : emailUses) {
			if (EntityCacheUtil.getResult(
						EmailUsModelImpl.ENTITY_CACHE_ENABLED,
						EmailUsImpl.class, emailUs.getPrimaryKey()) == null) {
				cacheResult(emailUs);
			}
			else {
				emailUs.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all email uses.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
			CacheRegistryUtil.clear(EmailUsImpl.class.getName());
		}

		EntityCacheUtil.clearCache(EmailUsImpl.class.getName());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the email us.
	 *
	 * <p>
	 * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(EmailUs emailUs) {
		EntityCacheUtil.removeResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsImpl.class, emailUs.getPrimaryKey());

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<EmailUs> emailUses) {
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (EmailUs emailUs : emailUses) {
			EntityCacheUtil.removeResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
				EmailUsImpl.class, emailUs.getPrimaryKey());
		}
	}

	/**
	 * Creates a new email us with the primary key. Does not add the email us to the database.
	 *
	 * @param id the primary key for the new email us
	 * @return the new email us
	 */
	@Override
	public EmailUs create(long id) {
		EmailUs emailUs = new EmailUsImpl();

		emailUs.setNew(true);
		emailUs.setPrimaryKey(id);

		return emailUs;
	}

	/**
	 * Removes the email us with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param id the primary key of the email us
	 * @return the email us that was removed
	 * @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs remove(long id)
		throws NoSuchEmailUsException, SystemException {
		return remove((Serializable)id);
	}

	/**
	 * Removes the email us with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the email us
	 * @return the email us that was removed
	 * @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs remove(Serializable primaryKey)
		throws NoSuchEmailUsException, SystemException {
		Session session = null;

		try {
			session = openSession();

			EmailUs emailUs = (EmailUs)session.get(EmailUsImpl.class, primaryKey);

			if (emailUs == null) {
				if (_log.isWarnEnabled()) {
					_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchEmailUsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(emailUs);
		}
		catch (NoSuchEmailUsException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected EmailUs removeImpl(EmailUs emailUs) throws SystemException {
		emailUs = toUnwrappedModel(emailUs);

		Session session = null;

		try {
			session = openSession();

			if (!session.contains(emailUs)) {
				emailUs = (EmailUs)session.get(EmailUsImpl.class,
						emailUs.getPrimaryKeyObj());
			}

			if (emailUs != null) {
				session.delete(emailUs);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (emailUs != null) {
			clearCache(emailUs);
		}

		return emailUs;
	}

	@Override
	public EmailUs updateImpl(com.blueoptima.model.EmailUs emailUs)
		throws SystemException {
		emailUs = toUnwrappedModel(emailUs);

		boolean isNew = emailUs.isNew();

		Session session = null;

		try {
			session = openSession();

			if (emailUs.isNew()) {
				session.save(emailUs);

				emailUs.setNew(false);
			}
			else {
				session.merge(emailUs);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (isNew) {
			FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}

		EntityCacheUtil.putResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
			EmailUsImpl.class, emailUs.getPrimaryKey(), emailUs);

		return emailUs;
	}

	protected EmailUs toUnwrappedModel(EmailUs emailUs) {
		if (emailUs instanceof EmailUsImpl) {
			return emailUs;
		}

		EmailUsImpl emailUsImpl = new EmailUsImpl();

		emailUsImpl.setNew(emailUs.isNew());
		emailUsImpl.setPrimaryKey(emailUs.getPrimaryKey());

		emailUsImpl.setId(emailUs.getId());
		emailUsImpl.setName(emailUs.getName());
		emailUsImpl.setEmail(emailUs.getEmail());
		emailUsImpl.setSubject(emailUs.getSubject());
		emailUsImpl.setMessage(emailUs.getMessage());

		return emailUsImpl;
	}

	/**
	 * Returns the email us with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the email us
	 * @return the email us
	 * @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs findByPrimaryKey(Serializable primaryKey)
		throws NoSuchEmailUsException, SystemException {
		EmailUs emailUs = fetchByPrimaryKey(primaryKey);

		if (emailUs == null) {
			if (_log.isWarnEnabled()) {
				_log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchEmailUsException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return emailUs;
	}

	/**
	 * Returns the email us with the primary key or throws a {@link com.blueoptima.NoSuchEmailUsException} if it could not be found.
	 *
	 * @param id the primary key of the email us
	 * @return the email us
	 * @throws com.blueoptima.NoSuchEmailUsException if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs findByPrimaryKey(long id)
		throws NoSuchEmailUsException, SystemException {
		return findByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns the email us with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the email us
	 * @return the email us, or <code>null</code> if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs fetchByPrimaryKey(Serializable primaryKey)
		throws SystemException {
		EmailUs emailUs = (EmailUs)EntityCacheUtil.getResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
				EmailUsImpl.class, primaryKey);

		if (emailUs == _nullEmailUs) {
			return null;
		}

		if (emailUs == null) {
			Session session = null;

			try {
				session = openSession();

				emailUs = (EmailUs)session.get(EmailUsImpl.class, primaryKey);

				if (emailUs != null) {
					cacheResult(emailUs);
				}
				else {
					EntityCacheUtil.putResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
						EmailUsImpl.class, primaryKey, _nullEmailUs);
				}
			}
			catch (Exception e) {
				EntityCacheUtil.removeResult(EmailUsModelImpl.ENTITY_CACHE_ENABLED,
					EmailUsImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return emailUs;
	}

	/**
	 * Returns the email us with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param id the primary key of the email us
	 * @return the email us, or <code>null</code> if a email us with the primary key could not be found
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public EmailUs fetchByPrimaryKey(long id) throws SystemException {
		return fetchByPrimaryKey((Serializable)id);
	}

	/**
	 * Returns all the email uses.
	 *
	 * @return the email uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EmailUs> findAll() throws SystemException {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the email uses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of email uses
	 * @param end the upper bound of the range of email uses (not inclusive)
	 * @return the range of email uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EmailUs> findAll(int start, int end) throws SystemException {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the email uses.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link com.blueoptima.model.impl.EmailUsModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of email uses
	 * @param end the upper bound of the range of email uses (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of email uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public List<EmailUs> findAll(int start, int end,
		OrderByComparator orderByComparator) throws SystemException {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<EmailUs> list = (List<EmailUs>)FinderCacheUtil.getResult(finderPath,
				finderArgs, this);

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 3));

				query.append(_SQL_SELECT_EMAILUS);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_EMAILUS;

				if (pagination) {
					sql = sql.concat(EmailUsModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<EmailUs>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = new UnmodifiableList<EmailUs>(list);
				}
				else {
					list = (List<EmailUs>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				FinderCacheUtil.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the email uses from the database.
	 *
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public void removeAll() throws SystemException {
		for (EmailUs emailUs : findAll()) {
			remove(emailUs);
		}
	}

	/**
	 * Returns the number of email uses.
	 *
	 * @return the number of email uses
	 * @throws SystemException if a system exception occurred
	 */
	@Override
	public int countAll() throws SystemException {
		Long count = (Long)FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_EMAILUS);

				count = (Long)q.uniqueResult();

				FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY, count);
			}
			catch (Exception e) {
				FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	protected Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	/**
	 * Initializes the email us persistence.
	 */
	public void afterPropertiesSet() {
		String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
					com.liferay.util.service.ServiceProps.get(
						"value.object.listener.com.blueoptima.model.EmailUs")));

		if (listenerClassNames.length > 0) {
			try {
				List<ModelListener<EmailUs>> listenersList = new ArrayList<ModelListener<EmailUs>>();

				for (String listenerClassName : listenerClassNames) {
					listenersList.add((ModelListener<EmailUs>)InstanceFactory.newInstance(
							getClassLoader(), listenerClassName));
				}

				listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
			}
			catch (Exception e) {
				_log.error(e);
			}
		}
	}

	public void destroy() {
		EntityCacheUtil.removeCache(EmailUsImpl.class.getName());
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	private static final String _SQL_SELECT_EMAILUS = "SELECT emailUs FROM EmailUs emailUs";
	private static final String _SQL_COUNT_EMAILUS = "SELECT COUNT(emailUs) FROM EmailUs emailUs";
	private static final String _ORDER_BY_ENTITY_ALIAS = "emailUs.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No EmailUs exists with the primary key ";
	private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
				PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
	private static Log _log = LogFactoryUtil.getLog(EmailUsPersistenceImpl.class);
	private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"id"
			});
	private static EmailUs _nullEmailUs = new EmailUsImpl() {
			@Override
			public Object clone() {
				return this;
			}

			@Override
			public CacheModel<EmailUs> toCacheModel() {
				return _nullEmailUsCacheModel;
			}
		};

	private static CacheModel<EmailUs> _nullEmailUsCacheModel = new CacheModel<EmailUs>() {
			@Override
			public EmailUs toEntityModel() {
				return _nullEmailUs;
			}
		};
}