package com.blueoptima.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.mail.internet.InternetAddress;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.blueoptima.model.EmailUs;
import com.blueoptima.service.EmailUsLocalServiceUtil;
import com.liferay.counter.service.CounterLocalServiceUtil;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.captcha.CaptchaUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;

@Controller(value = "EmailController")  
@RequestMapping("VIEW") 
public class EmailController {

	@RenderMapping  
	public String handleRenderRequest(RenderRequest request,RenderResponse response,Model model){ 
		String status = ParamUtil.getString(request, "status",StringPool.BLANK);
		String action = ParamUtil.getString(request, "action",StringPool.BLANK);
		
		request.setAttribute("status", status);
		request.setAttribute("action", action);
		return "view";  
	} 
	
	@ResourceMapping(value="captchaURL")
	public void setCaptcha(ResourceRequest request, ResourceResponse response){
		try {
			CaptchaUtil.serveImage(request, response);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@ResourceMapping(value="sendMailURL")
	public void sendMailURL(ResourceRequest resourceRequest,ResourceResponse resourceResponse,Model model) throws SystemException, IOException{
		HttpServletRequest request = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(resourceRequest));
		
		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");
		
		EmailUs emailUs = EmailUsLocalServiceUtil.createEmailUs(CounterLocalServiceUtil.increment(EmailUs.class.getName()));
		emailUs.setNew(true);
		emailUs.setName(name);
		emailUs.setEmail(email);
		emailUs.setSubject(subject);
		emailUs.setMessage(message);
		
		JSONObject emailObject = JSONFactoryUtil.createJSONObject();
		JSONObject resultObj = JSONFactoryUtil.createJSONObject();
		 
		if(Validator.isNull(name) || Validator.isNull(email) || Validator.isNull(subject) || Validator.isNull(message)){
			
			emailObject.put("name",name);
			emailObject.put("email",email);
			emailObject.put("subject",subject);
			emailObject.put("message",message);
			resultObj.put("status", "error");
			resultObj.put("emailObj", emailObject);
		}else{
				try{
					
					String enteredCaptchaText = request.getParameter("captchaText");
					PortletSession session = resourceRequest.getPortletSession();
					String captchaText = (String)getCaptchaValueFromSession(session);
			        if (Validator.isNull(enteredCaptchaText)) {
			        	emailObject.put("name",name);
						emailObject.put("email",email);
						emailObject.put("subject",subject);
						emailObject.put("message",message);
			    	   resultObj.put("status", "entercaptcha");
			    	   resultObj.put("emailObj", emailObject);
			        }else if(!captchaText.equals(enteredCaptchaText)){
			        	emailObject.put("name",name);
						emailObject.put("email",email);
						emailObject.put("subject",subject);
						emailObject.put("message",message);
			        	resultObj.put("status", "InvalidCaptcha");
			        	resultObj.put("emailObj", emailObject);
			        }else{
			        		try{
			        			MailMessage mailMessage=new MailMessage();
			        			mailMessage.setHTMLFormat(true);
			        			mailMessage.setBody(String.valueOf(htmlFormatedMailBody(name, email, subject, message)));
			        			mailMessage.setSubject("Contact Mail");
			        			mailMessage.setFrom(new InternetAddress("chiraggurav@gmail.com"));
			        			mailMessage.setTo(new InternetAddress("chirag.soni@scalsys.in"));
			        			MailServiceUtil.sendEmail(mailMessage);
					
					 
			        			// add contact entry in database if mail send successfully
			        			EmailUsLocalServiceUtil.updateEmailUs(emailUs);
			        			resultObj.put("status", "success");
			        			resultObj.put("emailObj", emailObject);
			        			}catch(Exception e1){
			        				model.addAttribute("emailUs",emailUs);
			        				resultObj.put("status", "error-send-mail");
			        				resultObj.put("emailObj", emailObject);
			        			}
			        }
			        }catch(Exception e){
		        	}
		}
			
		
		OutputStream os = resourceResponse.getPortletOutputStream();
		try{
			os.write(resultObj.toString().getBytes(StringPool.UTF8));
		}catch(IOException e){
			log.error(e.getMessage(),e);
		}finally{
			os.flush();
			os.close();
		}
	}


	private String getCaptchaValueFromSession(PortletSession session) {
		Enumeration<String> atNames = session.getAttributeNames();
        while (atNames.hasMoreElements()) {
            String name = atNames.nextElement();
            if (name.contains("CAPTCHA_TEXT")) {
                return (String) session.getAttribute(name);
            }
        }
        return null;
    
		
	}

	private StringBuilder htmlFormatedMailBody(String name, String email, String subject, String message) {
		StringBuilder mailBody = new StringBuilder();
			mailBody.append("<table border='1'>");
			mailBody.append("<tr>");
			mailBody.append("<th colspan='2'>EMAIL US</th>");
			mailBody.append("</tr>");
			mailBody.append("<tr>");
			mailBody.append("<td>Name</td>");
			mailBody.append("<td>" + name + "</td>");
			mailBody.append("</tr>");
			mailBody.append("<tr>");
			mailBody.append("<td>Email-ID</td>");
			mailBody.append("<td>"+ email +"</td>");
			mailBody.append("</tr>");
			mailBody.append("<tr>");
			mailBody.append("<td>Subject</td>");
			mailBody.append("<td>"+ subject +"</td>");
			mailBody.append("</tr>");
			mailBody.append("<tr>");
			mailBody.append("<td>Message</td>");
			mailBody.append("<td>"+ message +"</td>");
			mailBody.append("</tr>");
			mailBody.append("</table>");
			return mailBody;
	}
	private static Log log = LogFactoryUtil.getLog(EmailUs.class);
}
