/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.model.impl;

import com.blueoptima.model.EmailUs;
import com.blueoptima.model.EmailUsModel;
import com.blueoptima.model.EmailUsSoap;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the EmailUs service. Represents a row in the &quot;email_us&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link com.blueoptima.model.EmailUsModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link EmailUsImpl}.
 * </p>
 *
 * @author Scalsysu9
 * @see EmailUsImpl
 * @see com.blueoptima.model.EmailUs
 * @see com.blueoptima.model.EmailUsModel
 * @generated
 */
@JSON(strict = true)
public class EmailUsModelImpl extends BaseModelImpl<EmailUs>
	implements EmailUsModel {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. All methods that expect a email us model instance should use the {@link com.blueoptima.model.EmailUs} interface instead.
	 */
	public static final String TABLE_NAME = "email_us";
	public static final Object[][] TABLE_COLUMNS = {
			{ "id_", Types.BIGINT },
			{ "name", Types.VARCHAR },
			{ "email", Types.VARCHAR },
			{ "subject", Types.VARCHAR },
			{ "message", Types.VARCHAR }
		};
	public static final String TABLE_SQL_CREATE = "create table email_us (id_ LONG not null primary key,name VARCHAR(75) null,email VARCHAR(75) null,subject VARCHAR(75) null,message VARCHAR(75) null)";
	public static final String TABLE_SQL_DROP = "drop table email_us";
	public static final String ORDER_BY_JPQL = " ORDER BY emailUs.id ASC";
	public static final String ORDER_BY_SQL = " ORDER BY email_us.id_ ASC";
	public static final String DATA_SOURCE = "liferayDataSource";
	public static final String SESSION_FACTORY = "liferaySessionFactory";
	public static final String TX_MANAGER = "liferayTransactionManager";
	public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.entity.cache.enabled.com.blueoptima.model.EmailUs"),
			true);
	public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
				"value.object.finder.cache.enabled.com.blueoptima.model.EmailUs"),
			true);
	public static final boolean COLUMN_BITMASK_ENABLED = false;

	/**
	 * Converts the soap model instance into a normal model instance.
	 *
	 * @param soapModel the soap model instance to convert
	 * @return the normal model instance
	 */
	public static EmailUs toModel(EmailUsSoap soapModel) {
		if (soapModel == null) {
			return null;
		}

		EmailUs model = new EmailUsImpl();

		model.setId(soapModel.getId());
		model.setName(soapModel.getName());
		model.setEmail(soapModel.getEmail());
		model.setSubject(soapModel.getSubject());
		model.setMessage(soapModel.getMessage());

		return model;
	}

	/**
	 * Converts the soap model instances into normal model instances.
	 *
	 * @param soapModels the soap model instances to convert
	 * @return the normal model instances
	 */
	public static List<EmailUs> toModels(EmailUsSoap[] soapModels) {
		if (soapModels == null) {
			return null;
		}

		List<EmailUs> models = new ArrayList<EmailUs>(soapModels.length);

		for (EmailUsSoap soapModel : soapModels) {
			models.add(toModel(soapModel));
		}

		return models;
	}

	public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
				"lock.expiration.time.com.blueoptima.model.EmailUs"));

	public EmailUsModelImpl() {
	}

	@Override
	public long getPrimaryKey() {
		return _id;
	}

	@Override
	public void setPrimaryKey(long primaryKey) {
		setId(primaryKey);
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _id;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Class<?> getModelClass() {
		return EmailUs.class;
	}

	@Override
	public String getModelClassName() {
		return EmailUs.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("id", getId());
		attributes.put("name", getName());
		attributes.put("email", getEmail());
		attributes.put("subject", getSubject());
		attributes.put("message", getMessage());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long id = (Long)attributes.get("id");

		if (id != null) {
			setId(id);
		}

		String name = (String)attributes.get("name");

		if (name != null) {
			setName(name);
		}

		String email = (String)attributes.get("email");

		if (email != null) {
			setEmail(email);
		}

		String subject = (String)attributes.get("subject");

		if (subject != null) {
			setSubject(subject);
		}

		String message = (String)attributes.get("message");

		if (message != null) {
			setMessage(message);
		}
	}

	@JSON
	@Override
	public long getId() {
		return _id;
	}

	@Override
	public void setId(long id) {
		_id = id;
	}

	@JSON
	@Override
	public String getName() {
		if (_name == null) {
			return StringPool.BLANK;
		}
		else {
			return _name;
		}
	}

	@Override
	public void setName(String name) {
		_name = name;
	}

	@JSON
	@Override
	public String getEmail() {
		if (_email == null) {
			return StringPool.BLANK;
		}
		else {
			return _email;
		}
	}

	@Override
	public void setEmail(String email) {
		_email = email;
	}

	@JSON
	@Override
	public String getSubject() {
		if (_subject == null) {
			return StringPool.BLANK;
		}
		else {
			return _subject;
		}
	}

	@Override
	public void setSubject(String subject) {
		_subject = subject;
	}

	@JSON
	@Override
	public String getMessage() {
		if (_message == null) {
			return StringPool.BLANK;
		}
		else {
			return _message;
		}
	}

	@Override
	public void setMessage(String message) {
		_message = message;
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
			EmailUs.class.getName(), getPrimaryKey());
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		ExpandoBridge expandoBridge = getExpandoBridge();

		expandoBridge.setAttributes(serviceContext);
	}

	@Override
	public EmailUs toEscapedModel() {
		if (_escapedModel == null) {
			_escapedModel = (EmailUs)ProxyUtil.newProxyInstance(_classLoader,
					_escapedModelInterfaces, new AutoEscapeBeanHandler(this));
		}

		return _escapedModel;
	}

	@Override
	public Object clone() {
		EmailUsImpl emailUsImpl = new EmailUsImpl();

		emailUsImpl.setId(getId());
		emailUsImpl.setName(getName());
		emailUsImpl.setEmail(getEmail());
		emailUsImpl.setSubject(getSubject());
		emailUsImpl.setMessage(getMessage());

		emailUsImpl.resetOriginalValues();

		return emailUsImpl;
	}

	@Override
	public int compareTo(EmailUs emailUs) {
		long primaryKey = emailUs.getPrimaryKey();

		if (getPrimaryKey() < primaryKey) {
			return -1;
		}
		else if (getPrimaryKey() > primaryKey) {
			return 1;
		}
		else {
			return 0;
		}
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof EmailUs)) {
			return false;
		}

		EmailUs emailUs = (EmailUs)obj;

		long primaryKey = emailUs.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public void resetOriginalValues() {
	}

	@Override
	public CacheModel<EmailUs> toCacheModel() {
		EmailUsCacheModel emailUsCacheModel = new EmailUsCacheModel();

		emailUsCacheModel.id = getId();

		emailUsCacheModel.name = getName();

		String name = emailUsCacheModel.name;

		if ((name != null) && (name.length() == 0)) {
			emailUsCacheModel.name = null;
		}

		emailUsCacheModel.email = getEmail();

		String email = emailUsCacheModel.email;

		if ((email != null) && (email.length() == 0)) {
			emailUsCacheModel.email = null;
		}

		emailUsCacheModel.subject = getSubject();

		String subject = emailUsCacheModel.subject;

		if ((subject != null) && (subject.length() == 0)) {
			emailUsCacheModel.subject = null;
		}

		emailUsCacheModel.message = getMessage();

		String message = emailUsCacheModel.message;

		if ((message != null) && (message.length() == 0)) {
			emailUsCacheModel.message = null;
		}

		return emailUsCacheModel;
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(getId());
		sb.append(", name=");
		sb.append(getName());
		sb.append(", email=");
		sb.append(getEmail());
		sb.append(", subject=");
		sb.append(getSubject());
		sb.append(", message=");
		sb.append(getMessage());
		sb.append("}");

		return sb.toString();
	}

	@Override
	public String toXmlString() {
		StringBundler sb = new StringBundler(19);

		sb.append("<model><model-name>");
		sb.append("com.blueoptima.model.EmailUs");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>id</column-name><column-value><![CDATA[");
		sb.append(getId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>name</column-name><column-value><![CDATA[");
		sb.append(getName());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>email</column-name><column-value><![CDATA[");
		sb.append(getEmail());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>subject</column-name><column-value><![CDATA[");
		sb.append(getSubject());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>message</column-name><column-value><![CDATA[");
		sb.append(getMessage());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private static ClassLoader _classLoader = EmailUs.class.getClassLoader();
	private static Class<?>[] _escapedModelInterfaces = new Class[] {
			EmailUs.class
		};
	private long _id;
	private String _name;
	private String _email;
	private String _subject;
	private String _message;
	private EmailUs _escapedModel;
}