/**
 * Copyright (c) 2000-2013 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.blueoptima.model.impl;

import com.blueoptima.model.EmailUs;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing EmailUs in entity cache.
 *
 * @author Scalsysu9
 * @see EmailUs
 * @generated
 */
public class EmailUsCacheModel implements CacheModel<EmailUs>, Externalizable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(11);

		sb.append("{id=");
		sb.append(id);
		sb.append(", name=");
		sb.append(name);
		sb.append(", email=");
		sb.append(email);
		sb.append(", subject=");
		sb.append(subject);
		sb.append(", message=");
		sb.append(message);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public EmailUs toEntityModel() {
		EmailUsImpl emailUsImpl = new EmailUsImpl();

		emailUsImpl.setId(id);

		if (name == null) {
			emailUsImpl.setName(StringPool.BLANK);
		}
		else {
			emailUsImpl.setName(name);
		}

		if (email == null) {
			emailUsImpl.setEmail(StringPool.BLANK);
		}
		else {
			emailUsImpl.setEmail(email);
		}

		if (subject == null) {
			emailUsImpl.setSubject(StringPool.BLANK);
		}
		else {
			emailUsImpl.setSubject(subject);
		}

		if (message == null) {
			emailUsImpl.setMessage(StringPool.BLANK);
		}
		else {
			emailUsImpl.setMessage(message);
		}

		emailUsImpl.resetOriginalValues();

		return emailUsImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		id = objectInput.readLong();
		name = objectInput.readUTF();
		email = objectInput.readUTF();
		subject = objectInput.readUTF();
		message = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeLong(id);

		if (name == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(name);
		}

		if (email == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(email);
		}

		if (subject == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(subject);
		}

		if (message == null) {
			objectOutput.writeUTF(StringPool.BLANK);
		}
		else {
			objectOutput.writeUTF(message);
		}
	}

	public long id;
	public String name;
	public String email;
	public String subject;
	public String message;
}