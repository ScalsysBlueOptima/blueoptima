package com.blueoptima.recentcomments.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.service.ClassNameLocalServiceUtil;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.model.MBMessageConstants;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;

@Controller(value = "RecentCommentsController")
@RequestMapping("VIEW")
public class RecentCommentsController {

	@RenderMapping
	public String handleRenderRequest(RenderRequest request,RenderResponse response, Model model) throws SystemException {
		
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(MBMessage.class,PortalClassLoaderUtil.getClassLoader());
		dynamicQuery.add(PropertyFactoryUtil.forName("parentMessageId").ne(MBMessageConstants.DEFAULT_PARENT_MESSAGE_ID));
		dynamicQuery.add(PropertyFactoryUtil.forName("classNameId").eq(ClassNameLocalServiceUtil.getClassNameId(BlogsEntry.class)));
		dynamicQuery.addOrder(OrderFactoryUtil.desc("createDate"));
		dynamicQuery.setLimit(0, 5);
		
		List<MBMessage> blogsComments = MBMessageLocalServiceUtil.dynamicQuery(dynamicQuery);
		model.addAttribute("comments", blogsComments);
		
		return "view"; 
	}

}
