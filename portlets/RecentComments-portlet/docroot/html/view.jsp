<%@page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@page import="com.liferay.portal.kernel.util.StringBundler"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@include file="init.jsp"%>

<div class="cl" style="height:20px;"></div>
	<div class="blogheader02 clearfix">Recent Comments</div>
		<c:forEach items="${comments}" var="cmt">
			<c:set var="entryId" value="${cmt.classPK}"/>
				<%
					PortletURL showBlogEntryURL = renderResponse.createRenderURL();
					StringBundler sb = new StringBundler(8);
					sb.append(themeDisplay.getPathMain());
					sb.append("/blogs/find_entry?noSuchEntryRedirect=");
					sb.append(HttpUtil.encodeURL(showBlogEntryURL.toString()));
					sb.append("&entryId=");
					sb.append(pageContext.getAttribute("entryId"));
					String viewEntryURL = sb.toString();
				%>
	
				<ul class="ulsty05">
					<li><a href="<%=viewEntryURL%>">${cmt.body}</a></li>
				</ul>
			
		</c:forEach>
