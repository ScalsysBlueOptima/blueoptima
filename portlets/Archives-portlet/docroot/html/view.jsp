<%@include file="init.jsp"%>
<%@include file="view_js.jsp"%>

<div class="cl" style="height: 20px;"></div>

 <div class="blogheader02 clearfix">Archives</div>

	<select name='<portlet:namespace/>archiveMonthsList' id='<portlet:namespace/>archiveMonthsList' class="monthview" onchange="<portlet:namespace/>showBlogs();">
		<option value="">Show All</option>
		<c:forEach items="${listOfMonth}" var="month">
			<c:choose>
				<c:when test="${month == selectedMondth }">
					<option value="${month}" selected="selected">${month}</option>
				</c:when>
				<c:otherwise>
					<option value="${month}">${month}</option>
				</c:otherwise>
			</c:choose>
		</c:forEach>
	</select>
	
<div class="cl" style="height: 20px;"></div>