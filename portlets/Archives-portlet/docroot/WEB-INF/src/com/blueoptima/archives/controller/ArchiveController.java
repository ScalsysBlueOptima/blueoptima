package com.blueoptima.archives.controller;

import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;

@Controller(value = "ArchiveController")
@RequestMapping("VIEW")
public class ArchiveController {

	@RenderMapping
	public String handleRenderRequest(RenderRequest request, RenderResponse response, Model model) throws ParseException {
		try {
			List<BlogsEntry> allBlogs = BlogsEntryLocalServiceUtil.getBlogsEntries(-1, -1);
			List<Date>blogMonthAndYearList = new ArrayList<Date>();
			
			for (BlogsEntry blogsEntry : allBlogs) {
				blogMonthAndYearList.add(blogsEntry.getCreateDate());
			}
			
			Comparator cmp = Collections.reverseOrder(); 
			Collections.sort(blogMonthAndYearList, cmp); 
			
			List<String>blogMonthList = new ArrayList<String>();
			
			Calendar cal = Calendar.getInstance();
			for (Date date : blogMonthAndYearList) {
				cal.setTime(date);
				String monthYear = 	new DateFormatSymbols().getMonths()[cal.get(Calendar.MONTH)] + " " + cal.get(Calendar.YEAR);
				if(!blogMonthList.contains(monthYear)){
					blogMonthList.add(monthYear);
				}
			}
			
			String selectedMondth;
			HttpServletRequest httpRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
			selectedMondth = ParamUtil.get(httpRequest, "blogDate", StringPool.BLANK);
			
			request.setAttribute("selectedMondth", selectedMondth);
			request.setAttribute("listOfMonth", blogMonthList);
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return "view";
	}
	
	private int getMonthInInteger(Date createDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(createDate);
		return cal.get(Calendar.MONTH) + 1;
	}
}
