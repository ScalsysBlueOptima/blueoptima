<div class="sprow09 clearfix" id="main-content" role="main">
<div class="wrapper clearfix">
	<div class="blogmain clearfix">
		<div class="blogleft" id="column-1">
			$processor.processColumn("column-1", "portlet-column-content portlet-column-content-first")
		</div>
		<div class="blogright clearfix" id="column-2">
			$processor.processColumn("column-2", "portlet-column-content portlet-column-content-last")
		</div>
	</div>
	</div>
</div>
